
package ar.uba.fi.tdd.exercise;

class GildedRose {
    IUpdatableItem[] items;

    public GildedRose(IUpdatableItem[] _items) {
        items = _items;
    }

    // updates the quality of all items
    public void updateQuality() {
        for (int i = 0; i < items.length; i++) {

            items[i].updateQuality();
        }
    }

    // updates the sellIn of all items
    public void updateSellIn(){
        for (int i = 0; i < items.length; i++) {
            items[i].updateSellIn();
        }
    }
}
