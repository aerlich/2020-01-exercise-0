package ar.uba.fi.tdd.exercise;

public interface IUpdatableItem {

    void updateQuality();
    void updateSellIn();
    int getQuality();

 }
