package ar.uba.fi.tdd.exercise;

public class Item {

    //name
    public String Name;

    // seell in
    public int sellIn;

    //
    public int quality;

    // constructor
    public Item(String _name, int _sellIn, int _quality) {

        this.Name = _name;
        this.sellIn = _sellIn;
        this.quality = _quality;

        if (_quality < 0){
            this.quality = 0;
        }

        if(_quality > 50){
            this.quality = 50;
        }
    }

    public int getQuality(){
        return this.quality;
    }

    public boolean atMaxQuality() {
        return this.quality == 50;
    }

    public boolean atMinQuality() {
        return this.quality == 0;
    }

    public boolean hasExpired(){
        return this.sellIn < 0;
    }

    // shows the Item representation
   @Override
   public String toString() {

     return this.Name + ", " + this.sellIn + ", " + this.quality;
    }
 }
