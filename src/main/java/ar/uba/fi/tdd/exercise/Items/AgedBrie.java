package ar.uba.fi.tdd.exercise;

public class AgedBrie extends Item implements IUpdatableItem {

    public AgedBrie(int _sellIn, int _quality){
        super("Aged Brie", _sellIn, _quality);

    }

    public void updateQuality(){
        if (!this.atMaxQuality()) {
            this.quality += 1;
        }   

        if(this.hasExpired() && !this.atMaxQuality()){
            this.quality += 1;
        }
        
    }

    public void updateSellIn(){
        this.sellIn -= 1;
    }

 }
