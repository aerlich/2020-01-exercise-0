package ar.uba.fi.tdd.exercise;

public class BackstagePass extends Item implements IUpdatableItem {

    public BackstagePass(String _band, int _sellIn, int _quality){
        super("Backstage pass to a " + _band + " concert", _sellIn, _quality);

    }

    public void updateQuality(){
        // Backstage pass increases quality by 1 each day closer to sellin
        if (!this.atMaxQuality()) {
            this.quality += 1;
        }

        // Backstage pass increases quality by aditional 1 if theres 10 days or less remaining till sellIn
        if (this.sellIn < 11) {
            if (!this.atMaxQuality()) {
                this.quality += 1;
            }
        }

        // Backstage pass increases quality by aditional 1 if theres 6 days or less remaining till sellIn
        if (this.sellIn < 6) {
            if (!this.atMaxQuality()) {
                this.quality += 1;
            }
        }

        // Backstage pass quality drops to 0 once sellIn date has passed
        if(this.hasExpired()){
            this.quality = 0;
        }
        
    }

    public void updateSellIn(){
        this.sellIn -= 1;
    }

 }
