package ar.uba.fi.tdd.exercise;

public class Conjured extends Item implements IUpdatableItem {

    public Conjured(int _sellIn, int _quality){
        super("Conjured", _sellIn, _quality);

    }

    public void updateQuality(){
        // Conjured degrades twice as fast
        if(!this.atMinQuality()){
            this.quality -= 1;
        }

        if(!this.atMinQuality()){
            this.quality -= 1;
        }
    }

    public void updateSellIn(){
        this.sellIn -= 1;
    }

 }
