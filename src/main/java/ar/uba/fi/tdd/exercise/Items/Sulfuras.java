package ar.uba.fi.tdd.exercise;

public class Sulfuras extends Item implements IUpdatableItem {

    public Sulfuras(int _sellIn, int _quality){
        super("Sulfuras, Hand of Ragnaros", _sellIn, _quality);

    }

    public void updateQuality(){
        // Sulfuras dont lose quality
    }

    public void updateSellIn(){
        // Sulfuras dont expire
    }

 }
