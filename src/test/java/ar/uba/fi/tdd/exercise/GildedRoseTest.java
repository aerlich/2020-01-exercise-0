package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GildedRoseTest {

	@Test
	public void AgedBrieQualityUpdate() {
		IUpdatableItem[] items = new IUpdatableItem[] { new AgedBrie(-1, 0) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(2).isEqualTo(app.items[0].getQuality());
	}

	@Test
	public void AgedBrieQualityUpdate2() {
		IUpdatableItem[] items = new IUpdatableItem[] { new AgedBrie(0, 0) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(1).isEqualTo(app.items[0].getQuality());
	}

	@Test
	public void BackstagePassQualityUpdate() {
		IUpdatableItem[] items = new IUpdatableItem[] { new BackstagePass("TAFKAL80ETC", -1, 0) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(0).isEqualTo(app.items[0].getQuality());
	}

	@Test
	public void BackstagePassQualityUpdate1() {
		IUpdatableItem[] items = new IUpdatableItem[] { new BackstagePass("TAFKAL80ETC", 1, 0) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(3).isEqualTo(app.items[0].getQuality());
	}

	@Test
	public void BackstagePassQualityUpdate2() {
		IUpdatableItem[] items = new IUpdatableItem[] { new BackstagePass("TAFKAL80ETC", 7, 0) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(2).isEqualTo(app.items[0].getQuality());
	}

	@Test
	public void BackstagePassQualityUpdate3() {
		IUpdatableItem[] items = new IUpdatableItem[] { new BackstagePass("TAFKAL80ETC", 11, 0) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(1).isEqualTo(app.items[0].getQuality());
	}

	@Test
	public void SulfurasQualityUpdate() {
		IUpdatableItem[] items = new IUpdatableItem[] { new Sulfuras(5, 4) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(4).isEqualTo(app.items[0].getQuality());
	}

	@Test
	public void ConjuredQualityUpdate() {
		IUpdatableItem[] items = new IUpdatableItem[] { new Conjured(5, 4) };
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(2).isEqualTo(app.items[0].getQuality());
	}

	

}
